﻿using UnityEngine;

namespace Game
{
    public class Dog : MonoBehaviour
    {
        private PlayerInputs playerInputs;
        private PlayerMover playerMover;
        private RotateLightSource rotateLightSource;

        private void Awake()
        {
            playerInputs = Finder.PlayerInputs;
            playerMover = GetComponent<PlayerMover>();
            rotateLightSource = GetComponent<RotateLightSource>();
        }

        private void Update()
        {
            Vector2 move = playerInputs.Game.Movement.ReadValue<Vector2>();
            playerMover.Move(move);
            rotateLightSource.Rotate(move);
        }
    }
}