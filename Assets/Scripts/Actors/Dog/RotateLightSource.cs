﻿using System;
using System.Collections;
using Game;
using Unity.Mathematics;
using UnityEngine;

namespace Game
{
    public class RotateLightSource : MonoBehaviour
    
    {
        private LightingSource2D lightingSource2D;

        private void Start()
        {
            lightingSource2D = GetComponentInChildren<LightingSource2D>();
        }
        
        public void Rotate(Vector2 direction)
        {
            lightingSource2D.transform.up = -direction;
        }
    }
}