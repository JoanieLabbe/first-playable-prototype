﻿using System;
using UnityEngine;

namespace Game
{
    public class HearStimulus : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var otherGameObject = other.gameObject;
            var hearable = otherGameObject.GetComponent<IHearable>();
            if (hearable != null)
            {
                hearable.Hear();
            }
        }
    }
}