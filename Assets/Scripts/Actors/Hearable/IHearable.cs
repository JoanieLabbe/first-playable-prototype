﻿namespace Game
{
    public interface IHearable
    {
        void Hear();
    }
}