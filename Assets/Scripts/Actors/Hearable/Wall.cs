﻿using System.Collections;
using UnityEngine;

namespace Game
{
    public class Wall : MonoBehaviour, IHearable
    {
        private GameObject lightObject;
        private bool isSeen = false;
        private LightingSource2D lightSource;
        [SerializeField] private float maxLightSize = 2f;
        [SerializeField] private float minLightSize = 0f;
        [SerializeField] private float delay = 5f;

        private void Start()
        {
            lightSource = gameObject.GetComponentInChildren<LightingSource2D>();
            lightSource.lightSize = 0;
        }

        public void Hear()
        {
            if (!isSeen)
            {
                StartCoroutine(MakeLigthVisibleRoutine());
            }
        }

        IEnumerator MakeLigthVisibleRoutine()
        {
            isSeen = true;
            lightSource.lightSize = maxLightSize;
            yield return new WaitForSeconds(delay);
            lightSource.lightSize = minLightSize;
            isSeen = false;
        }
    }
}