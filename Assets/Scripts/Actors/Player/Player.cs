﻿using UnityEngine;

namespace Game
{
    public class Player : MonoBehaviour
    {
        private PlayerInputs playerInputs;
        private PlayerMover playerMover;

        private void Awake()
        {
            playerInputs = Finder.PlayerInputs;
            playerMover = GetComponent<PlayerMover>();
        }

        private void Update()
        {
            playerMover.Move(playerInputs.Game.Movement.ReadValue<Vector2>());
        }
    }
}