﻿using UnityEngine;

namespace Game
{
    public class PlayerInputs : MonoBehaviour
    {
        private InputsActions inputs;
        
        public InputsActions.GameActions Game => inputs.Game;
        
        private void Awake()
        {
            inputs = new InputsActions();
        }

        private void Start()
        {
            inputs.Game.Enable();
        }
    }
}