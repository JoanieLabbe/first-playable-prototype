﻿using UnityEngine;

namespace Game
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] private const float SPEED = 2f;
        public void Move(Vector2 movement)
        {
            transform.Translate(movement * (SPEED * Time.deltaTime));
        }
    }
}