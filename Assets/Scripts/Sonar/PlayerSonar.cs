﻿using System.Collections;
using UnityEngine;

namespace Game
{
    public class PlayerSonar : MonoBehaviour
    {
        [SerializeField] private float spawnDelay = 2f;
        [SerializeField] private GameObject prefab;
        
        private void OnEnable()
        {
            StartCoroutine(SpawnRoutine());
        }

        private IEnumerator SpawnRoutine()
        {
            while (isActiveAndEnabled)
            {
                ObjectPooler.EnableObject(Tags.SONAR, transform.position, Quaternion.identity);
                yield return new WaitForSeconds(spawnDelay);
            }
        }
    }
}