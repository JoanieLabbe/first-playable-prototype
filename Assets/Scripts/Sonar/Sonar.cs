﻿using System;
using UnityEngine;

namespace Game
{
    public class Sonar : MonoBehaviour
    {
        [SerializeField] private float maxRadius = 5f;
        private float minRadius = 0f;
        private float changeRadius = 0.01f;

        private float radius;
        private CircleCollider2D circleCollider2D;
        private LightingSource2D lightingSource2D;

        private float Radius
        {
            get => radius;
            set
            {
                radius = value;
                circleCollider2D.radius = radius;
                lightingSource2D.lightSize = radius * 2;
            }
        }

        void Start ()
        {
            circleCollider2D = gameObject.GetComponent<CircleCollider2D>();
            lightingSource2D = gameObject.GetComponent<LightingSource2D>();
        }

        private void Update()
        {
            Radius += changeRadius;
            if (Radius >= maxRadius)
            {
                Radius = minRadius;
                ObjectPooler.DisableObject(gameObject);
            }
        }
    }
}