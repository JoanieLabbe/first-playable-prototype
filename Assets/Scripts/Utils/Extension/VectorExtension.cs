﻿using System;
using UnityEngine;

namespace Game
{
    public static class VectorExtension
    {
        public static bool IsEquals(this Vector3 vector, Vector3 vectorToCompare, float threshold)
        {
            if (Math.Abs(vector.x - vectorToCompare.x) <= threshold
                && Math.Abs(vector.y - vectorToCompare.y) <= threshold
                && Math.Abs(vector.z - vectorToCompare.z) <= threshold)
            {
                return true;
            }
            return false;
        }
        
        public static bool IsEquals(this Vector2 vector, Vector2 vectorToCompare, float threshold)
        {
            if (Math.Abs(vector.x - vectorToCompare.x) <= threshold
                && Math.Abs(vector.y - vectorToCompare.y) <= threshold)
            {
                return true;
            }
            return false;
        }
    }
}