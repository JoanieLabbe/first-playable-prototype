﻿using UnityEngine;

namespace Game
{
    public static class Finder
    {
        private static GameObject globals;
        private static PlayerInputs playerInputs;
        private static ObjectPooler objectPooler;
        
        private static GameObject Globals
        {
            get
            {
                if (globals == null)
                {
                    globals = GameObject.FindWithTag(Tags.GLOBALS);
                }

                return globals;
            }
        }
        
        public static ObjectPooler ObjectPooler
        {
            get
            {
                if (objectPooler == null)
                {
                    objectPooler = Globals.GetComponent<ObjectPooler>();
                }

                return objectPooler;
            }
        }

        public static PlayerInputs PlayerInputs
        {
            get
            {
                if (playerInputs == null)
                {
                    playerInputs = Globals.GetComponent<PlayerInputs>();
                }

                return playerInputs;
            }
        }
    }
}